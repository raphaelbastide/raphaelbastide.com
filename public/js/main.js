
// Toggle for the main menu
var toggle = document.querySelectorAll(".toggle");
for(var z = 0; z < toggle.length; z++){
  var elem = toggle[z];
  elem.onclick = function(){
    var btnId = this.id,
        boxId = btnId.split('-btn')[0],
        box = document.getElementById(boxId);
    if (box.classList.contains("opened")) {
      closeall();
    }else {
      closeall();
      this.classList.add("has-opened");
      box.classList.add("opened");
    }
    return false;
  };
}

// Toggle projects info
var toggle = document.querySelectorAll("figure .desc-btn");
for(var z = 0; z < toggle.length; z++){
  var elem = toggle[z];
  elem.onclick = function(){
    var header = this.parentNode.parentNode;
    if (header.classList.contains("opened")) {
      this.classList.remove("has-opened");
      header.classList.remove("opened");
    }else {
      this.classList.add("has-opened");
      header.classList.add("opened");
    }
    return false;
  };
}

function closeall(){
  var toggle = document.querySelectorAll(".toggle");
      box = document.querySelectorAll(".sub-menu");
      console.log(box);
  for(var z = 0; z < toggle.length; z++){
    toggle[z].classList.remove("has-opened")
  }
  for(var z = 0; z < box.length; z++){
    box[z].classList.remove("opened")
  }
}

window.onscroll = function(ev) {
  var w = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
  if (w >= 1000) {
    closeall();
  }
};

// Append bio and credits in header
var biobox = document.getElementById('biobox'),
    biohtml = document.getElementById("bio").innerHTML,
    credbox = document.getElementById('credbox'),
    credhtml = document.getElementById("cred").innerHTML;

appendHtml(biobox, biohtml);
appendHtml(credbox, credhtml);
document.getElementById("bio").remove();
document.getElementById("cred").remove();

function appendHtml(el, str) {
  var div = document.createElement('div');
  div.innerHTML = str;
  console.log(str);
  while (div.children.length > 0) {
    el.appendChild(div.children[0]);
  }
}

// Lazi load

[].forEach.call(document.querySelectorAll('img[data-src]'), function(img) {
  img.setAttribute('src', img.getAttribute('data-src'));
  img.onload = function() {
    img.removeAttribute('data-src');
  };
});
